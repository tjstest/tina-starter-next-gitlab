import { getGitlabPreviewProps } from "next-tinacms-gitlab";
import fs from "fs";

export const getJsonPageProps = async (
  preview: boolean,
  previewData: any,
  filePath: string
) => {
  if (preview) {
    const previewProps = await getGitlabPreviewProps({
      ...(previewData as any),
      fileRelativePath: filePath,
      parse: JSON.parse,
    });
    return {
      props: {
        ...previewProps.props,
        file: {
          fileName: filePath
            .substring(filePath.lastIndexOf("/"))
            .replace(".json", ""),
          fileRelativePath: filePath,
          data: previewProps.props.file?.data,
        },
      },
    };
  } else {
    return {
      props: {
        error: null,
        preview: false,
        file: {
          fileName: filePath
            .substring(filePath.lastIndexOf("/"))
            .replace(".json", ""),
          fileRelativePath: filePath,
          data: JSON.parse(fs.readFileSync(filePath, "utf8")),
        },
      },
    };
  }
};
