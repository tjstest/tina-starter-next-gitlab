import fastGlob from "fast-glob";

export const getMarkdownPagePaths = async (directory: string) => {
  const pages = await fastGlob(`${directory}/*.md`);

  const slugs = pages.map((fileName) =>
    fileName
      .replace(new RegExp(`^${escape(directory)}\/|\.md$`, "g"), "")
      .replace(/ /g, "-")
  );

  const paths = slugs.map((slug) => ({ params: { slug: slug } }));

  return {
    paths: paths,
    fallback: !process.env.NEXT_PUBLIC_STATIC,
  };
};

const escape = (string) => {
  return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
};
