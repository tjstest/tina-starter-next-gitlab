import { getFiles } from "next-tinacms-gitlab";
import fastGlob from "fast-glob";
import { getJsonPageProps } from "./getJsonPageProps";

export const getListJsonPageProps = async (
  preview: boolean,
  previewData: any,
  directory: string
) => {
  const files = preview
    ? await getFiles(
        directory,
        previewData.working_project_id,
        previewData.head_branch,
        previewData.gitlab_access_token
      )
    : await fastGlob(`${directory}/*.json`);

  const pages = await Promise.all(
    files.map(async (file) => {
      const { props } = await getJsonPageProps(preview, previewData, file);
      return props.file;
    })
  );

  return {
    props: {
      pages,
      preview: preview || false,
    },
  };
};
