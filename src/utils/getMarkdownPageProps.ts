import { getGitlabPreviewProps, parseMarkdown } from "next-tinacms-gitlab";
import fs from "fs";

export const getMarkdownPageProps = async (
  preview: boolean,
  previewData: any,
  filePath: string
) => {
  if (preview) {
    const previewProps = await getGitlabPreviewProps({
      ...(previewData as any),
      fileRelativePath: filePath,
      parse: parseMarkdown,
    });
    return {
      props: {
        ...previewProps.props,
        file: {
          fileName: filePath
            .substring(filePath.lastIndexOf("/"))
            .replace(".md", ""),
          fileRelativePath: filePath,
          data: previewProps.props.file?.data,
        },
      },
    };
  } else {
    return {
      props: {
        error: null,
        preview: false,
        file: {
          fileName: filePath
            .substring(filePath.lastIndexOf("/"))
            .replace(".md", ""),
          fileRelativePath: filePath,
          data: parseMarkdown(fs.readFileSync(filePath, "utf8")),
        },
      },
    };
  }
};
