import { useRouter } from "next/router";
import { ContentCreatorPlugin, usePlugin } from "tinacms";
import { useGitlabClient } from "react-tinacms-gitlab";

export const useCreatePagePlugin = () => {
  const router = useRouter();
  const gitlab = useGitlabClient();
  const trimmedPath = router.asPath.replace(/^\/|\/$/g, "");
  const pageCreatorPlugin: ContentCreatorPlugin<any> = {
    __type: "content-creator",
    name: "New Page",
    fields: [
      {
        label: "Slug",
        name: "slug",
        component: "text",
        validate(value) {
          if (!value || value === "") {
            return "Required";
          }
          if (!RegExp(`^[a-z0-9]+(?:-[a-z0-9]+)*$`).test(value)) {
            return "Invalid URL path";
          }
        },
      },
      {
        label: "Title",
        name: "title",
        component: "text",
        validate: (value) => {
          if (!value || value === "") {
            return "Required";
          }
        },
      },
    ],
    onSubmit: async (formData) => {
      const slug = formData.slug;
      const fileRelativePath = `content/pages/${trimmedPath}/${slug}.json`;
      return await gitlab
        .upload(
          fileRelativePath,
          JSON.stringify({
            title: formData.title || "",
            description: "",
          }),
          `Created page /${trimmedPath}/${slug}`
        )
        .then(() => {
          setTimeout(() => router.push(`/${trimmedPath}/${slug}`), 1500);
        });
    },
  };
  usePlugin(pageCreatorPlugin);
};
