import React, { FunctionComponent } from "react";
import { useGitlabToolbarPlugins } from "react-tinacms-gitlab";
import { useCMS } from "tinacms";
import { useCreatePagePlugin } from "../hooks/useCreatePagePlugin";

export const Layout: FunctionComponent = ({ children }) => {
  const cms = useCMS();
  useGitlabToolbarPlugins();
  useCreatePagePlugin();

  return (
    <div>
      <button onClick={() => cms.toggle()}>
        {cms.enabled ? "Stop editing" : "Start editing"}
      </button>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
