import React from "react";
import { TinaCMS, TinaProvider } from "tinacms";
import Layout from "../src/components/layout";
import App from "next/app";
import { GitlabClient, TinacmsGitlabProvider } from "react-tinacms-gitlab";
import { NextGitlabMediaStore } from "next-tinacms-gitlab";
import { createGlobalStyle } from "styled-components";

const gitlabClient = new GitlabClient({
  projectId: process.env.NEXT_PUBLIC_GITLAB_PROJECT_ID,
  clientId: process.env.NEXT_PUBLIC_GITLAB_CLIENT_ID,
  baseBranch: process.env.NEXT_PUBLIC_GITLAB_BASE_BRANCH,
  redirectURI: process.env.NEXT_PUBLIC_GITLAB_REDIRECT_URI,
  proxy: "/api/proxy-gitlab",
  authCallbackRoute: "/api/create-gitlab-access-token",
});

class MyApp extends App {
  private readonly cms: TinaCMS;
  constructor(props) {
    super(props);

    this.cms = new TinaCMS({
      enabled: props.pageProps?.preview,
      apis: {
        gitlab: gitlabClient,
      },
      media: new NextGitlabMediaStore(gitlabClient),
      toolbar: props.pageProps?.preview,
      sidebar: {
        position: "displace",
        placeholder: () => {
          return (
            <div style={{ padding: 20 }}>
              <p>This page is not editable.</p>
            </div>
          );
        },
      },
    });
  }

  render() {
    const { Component, pageProps } = this.props;

    import("react-tinacms-editor").then(({ MarkdownFieldPlugin }) => {
      this.cms.plugins.add(MarkdownFieldPlugin);
    });

    return (
      <TinaProvider cms={this.cms}>
        <TinacmsGitlabProvider
          onLogin={onLogin}
          onLogout={onLogout}
          error={pageProps.error}
        >
          <GlobalStyles />
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </TinacmsGitlabProvider>
      </TinaProvider>
    );
  }
}

const GlobalStyles = createGlobalStyle`
  html,body {
    margin: 0;
    padding: 0;
  }
`;

async function onLogin() {
  const token = localStorage.getItem("tinacms-gitlab-token") || null;
  const headers = new Headers();

  if (token) {
    headers.append("Authorization", "Bearer " + token);
  }

  const resp = await fetch(`/api/preview`, { headers: headers });
  const data = await resp.json();

  if (resp.status == 200) window.location.href = window.location.pathname;
  else throw new Error(data.message);
}

async function onLogout() {
  await fetch(`/api/reset-preview`);
  window.location.reload();
}

export default MyApp;
