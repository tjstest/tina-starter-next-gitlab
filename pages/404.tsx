import React from "react";
import SEO from "../src/components/seo";

export default function Custom404Page() {
  return (
    <>
      <SEO title="Not found" />
      <h2>404: Page Not Found</h2>
    </>
  );
}
