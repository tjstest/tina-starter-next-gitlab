import { useGitlabAuthRedirect } from "react-tinacms-gitlab";

export default function Authorizing() {
  useGitlabAuthRedirect();
  return <h2>Authorizing with Gitlab, please wait...</h2>;
}
