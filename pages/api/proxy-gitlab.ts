import { apiProxy } from "next-tinacms-gitlab";
import getConfig from "next/config";

export default apiProxy(getConfig().serverRuntimeConfig.signingKey);
