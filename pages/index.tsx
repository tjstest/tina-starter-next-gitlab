import React from "react";
import SEO from "../src/components/seo";
import Link from "next/link";
import { GetStaticProps } from "next";

export default function HomePage() {
  return (
    <>
      <SEO title="Home" description="Tina Starter NextJS Gitlab" />
      <h1>Home</h1>
      <Link key="json" href="/json">
        <a>
          <h4>JSON</h4>
        </a>
      </Link>
      <Link key="markdown" href="/markdown">
        <a>
          <h4>Markdown</h4>
        </a>
      </Link>
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({
  preview,
  previewData,
}) => {
  return {
    props: {
      preview: preview || false,
      previewData: previewData || null,
    },
  };
};
